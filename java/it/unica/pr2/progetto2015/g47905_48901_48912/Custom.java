package it.unica.pr2.progetto2015.g47905_48901_48912;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.nio.charset.Charset;
import java.io.FileNotFoundException;
import java.io.*;


public class Custom implements SheetFunction {

	public Object execute(Object... args) {
	
		String inputUrl = (String) args [0];
		String inputScelta = (String) args [1];
		int scelta = Integer.parseInt(inputScelta); 
		String risultato = "";
		
		try{
			Document doc = Jsoup.connect(inputUrl).get();
			switch (scelta) {
				case 1 :	
					risultato = "Titolo: " + doc.title();
					break;
				case 2 :
					Charset nome = doc.charset();
					risultato = "Charset: " + nome.name();
					break;
				case 3 :
					Element link = doc.select("a").first();
					risultato = "Primo link: " + link.attr("href");
					break;
				case 4 :
					risultato = doc.outerHtml();
					break;
				default :
					risultato = "Errore Scelta";
					break;
				}
			}
		catch (IOException e){
			risultato = "URL non corretto oppure sito non raggiungibile";
		}
		
		return (String) risultato;
	
	}
	
	public final String getCategory(){
		return "CUSTOM";
	}

	public final String getHelp(){
		return "La presente funzione Custom permette di utilizzare la libreria esterna Jsoup. Inserendo un URL è possibile recuperare varie informazioni riguardanti la pagina. 1-Titolo 2-Charset 3-Primo link della pagina 4-Recupero della pagina e stampa a video del codice html";
	}
	 
	public final String getName(){
		return "CUSTOM";
	}
	


}
