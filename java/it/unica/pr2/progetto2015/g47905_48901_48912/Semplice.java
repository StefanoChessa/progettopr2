package it.unica.pr2.progetto2015.g47905_48901_48912;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;

public class Semplice  implements SheetFunction{
  
    @Override
    public Object execute(Object... args){
        Double valore = (Double) args [0];
  		valore = Math.sin(valore);  	
        return valore;
    }
 
    @Override
    public final String getCategory(){
        return "MATEMATICA"; 
    }

    @Override
    public final String getHelp(){
        return "Restituisce il seno di un determinato angolo espresso in radianti.";
    }
              
    @Override
    public final String getName(){
        return "SEN";
    }
}
