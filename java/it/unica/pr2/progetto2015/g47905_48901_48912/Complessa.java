package it.unica.pr2.progetto2015.g47905_48901_48912;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;

public class Complessa  implements SheetFunction{
  
    @Override
    public Object execute(Object... args){
    		Double [][] vett1 = ((Double[][]) args[0]);
    		Double [][] vett2 = ((Double[][]) args[1]);
    		Double valore = new Double (0);
    		    		
    		for (int i = 0; i < vett1.length ; i++){
    			for(int j=0; j< vett1[i].length; j++ ) {
    				valore += Math.pow(vett1[i][j],2) - Math.pow(vett2[i][j],2);
    			}
    		}
    		
    		return (Double)valore;
		}
 
    @Override
    public final String getCategory(){
        return "MATRICE"; 
    }

 
    @Override
    public final String getHelp(){
        return "Restituisce la somma della differenza dei quadrati dei valori corrispondenti delle due matrici.";
       			
    }        
     
    @Override
    public final String getName(){
        return "SOMMA.DIFF.Q";
    }
}
