# Progetto di Programmazione 2 2014-2015

## Team: ##
* Stefano R. Chessa
* Marco Guria
* Giovanni Usai

## Funzione Semplice: Seno (SEN) ##
La funzione restituisce il seno dell'angolo inserito, espresso in radianti.

## Comandi e istruzioni ##
* 5 campi sul foglio elettronico, Dato - Originale - Oggetto - Chiamata - Risultato.
* Inserire nel campo "Dato" il valore dell'angolo, espresso in radianti, di cui si desidera calcolare Il seno.
* In corrispondenza del campo "Originale" viene mostrato il risultato della funzione SEN di LibreOffice.
* All'interno del campo "Oggetto" viene creato un "oggettoSen" proveniente dalla classe "Semplice" implementata in Java.
* Sul campo "Chiamata" viene chiamato il metodo "execute" della funzione "Semplice", fornendogli come parametro in input il valore inserito    	 all'interno del campo "Dato".
* Il risultato viene visualizzato in corrispondenza del campo "Risultato".

## Funzione complessa: SOMMA.DIFF.Q ##
La funzione prende in input due matrici, e restituisce la somma della differenza dei quadrati dei valori corrispondenti delle due matrici.

## Comandi e istruzioni ##
* 5 campi sul foglio elettronico, Dati - Originale - Oggetto - Chiamata - Risultato.
* Inserire in corrispondenza del campo "Dati" i valori che si vogliono assegnare alle due matrici, dalla cella A6 ad A8 per MatrX e dalla     	cella B6 a B8 per MatrY. 
* In corrispondenza del campo "Originale" viene mostrato il risultato della funzione SOMMA.DIFF.Q di LibreOffice.
* All'interno del campo "Oggetto" viene creato un "oggettoComplessa" proveniente dalla classe "Complessa" implementata in Java.
* Sul campo "Chiamata" viene chiamato il metodo "execute" della funzione "Complessa", fornendogli come parametri in input i valori inseriti    	 all'interno del campo "Dati".
* Il risultato viene visualizzato in corrispondenza del campo "Risultato".

## Funzione Custom: utilizzo libreria esterna Jsoup ##
La funzione permette di utilizzare la libreria esterna Jsoup. Inserendo un URL è possibile recuperare varie informazioni riguardanti la pagina. 1-Titolo 2-Charset 3-Primo link della pagina 4-Recupero della pagina e stampa sul foglio del codice html.

## Comandi e istruzioni ##
* 4 campi sul foglio elettronico, Dati - Oggetto - Chiamata - Risultato.
* Inserire in corrispondenza di "Inserisci URL" l'indirizzo URL dal quale si vogliono recuperare informazioni.
* Inserire in corrispondenza di "Inscerisci scelta" un valore da 1 a 4 in base all'informazione desiderata. 1 per il titolo, 2 per il charset, 3 per il primo link della pagina e 4 per il recupero della pagina e la stampa sul foglio del codice html. 
* All'interno del campo "Oggetto" viene creato un "oggettoCustom" proveniente dalla classe "Custom" implementata in Java.
* Sul campo "Chiamata" viene chiamato il metodo "execute" della funzione "Custom", fornendogli come parametri in input l'URL e              la scelta inserita.
* Il risultato viene visualizzato in corrispondenza del campo "Risultato", in base alla scelta effettuata verrà visualizzata 	            l'informazione desiderata.


